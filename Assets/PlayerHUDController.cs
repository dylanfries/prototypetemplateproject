﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Added use of Text and other UI elements
using UnityEngine.UI;

public class PlayerHUDController : MonoBehaviour {

    public Text coinsPrintout;
	
	// Update is called once per frame
	public void UpdateUI (PlayerController currentPlayer) {
        if(currentPlayer == null){
            Debug.LogWarning("Passed a Null Player to the HUD UI");
        }else{
            coinsPrintout.text = currentPlayer.collectedCoins.ToString();
        }
	}
}
