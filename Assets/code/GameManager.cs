﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// The game manager controls the flow of execution of the overall game 
// Respawns the player
// Begin and end games
// keep score (for now)
public class GameManager : MonoBehaviour {

    [Header("This is the Player Object Template, set it")]
    public PlayerController playerPrefabToSpawn;
    [Header("Set This To a Location")]
    // If we want to spawn the player at a specific spot, place a gameobject here")]
    public Transform spawnPoint;

    // This is the current active player
    private PlayerController currentPlayer;

    [Header("Game Settings")]
    public int playerLives = 4;
    public int numberOfLevels = 3;
    [Header("Player State and Settings")]
    public int currentLivesLeft = 0;
    public int currentRoundCounter = 0;
    public int currentScore = 0;

    [Header("Player UI")]
    public PlayerHUDController hud;

    public List<Transform> levelPrefabs;
    //private Transform currentRound;

    [Header("Debug")]
    public bool debugLogs = true;

    // Use this for initialization
    void Start() {
        // Starts a new round
        StartGame();
    }

    // creates a new player
    // does a player have multiple lives? We are now assuming the player can be destroyed as well
    public void RespawnPlayer(){
        // **** ERROR CHECKING ****
        if(debugLogs ){ Debug.Log("Respawning"); }
        // xxx Error checking
        if ( playerPrefabToSpawn == null){
            Debug.LogWarning("You need to add a player prefab to the Game Manager");
            return;
        }
        // XXX Make sure we have a spawn point
        if( spawnPoint == null){
            spawnPoint = this.transform;
            Debug.LogWarning("No spawn point found so we're spawning at the Game Manager Position");
        }

        // *** Player Lives & Check for end of game
        // if player has more then 0 lives
        if (playerLives > 0) {
            // creates a new player. The template is 'playerPrefabToSpawn', the location and rotation are based on spawnPoint, and this Game Manager is set as the Players parent
            currentPlayer = Instantiate<PlayerController>(playerPrefabToSpawn, spawnPoint.position, spawnPoint.rotation, transform);
            currentPlayer.Initialize(this); // pass the manager reference to the player
            playerLives--; // removes one life from the player

            if( hud != null){
                hud.UpdateUI(currentPlayer);
            } else{
                Debug.LogWarning("No HUD found on Game Manager");
            }

        // no player lives are left
        } else{
            // End the games
            GameOver();
        }
    }

    // ***** ROUND CONTROLS *****
    public void CreateRound(int roundId){
        //// **** ERROR CHECKING ****
        //// if the list exists
        //if (levelPrefabs== null){
        //    Debug.LogWarning("No level Prefabs found to spawn");
        //    return;
        //}
        
        //// if we have a round to spawn
        //if(roundId >= levelPrefabs.Count){
        //    Debug.LogWarning("No level with that ID. Make another Level!");
        //    return;
        //}
        
        //// check that the list entry isnt' null
        //if( levelPrefabs[roundId] == null){
        //    Debug.LogWarning("Round to Spawn is null, check your list in GameController");
        //    return;
        //}

        //// Method 1 - Destroy and Respawn
        //// Destroy the list round
        //Destroy(currentRound);

        //// create a new round
        //currentRound = Instantiate(levelPrefabs[roundId], transform.position, transform.rotation, transform);

        // Method 2 - Just enable and disable the others
        // These rounds must be in scene references
        EnableRound(roundId);
    }

    public void EnableRound(int roundNumber){
        foreach(Transform r in levelPrefabs){
            r.gameObject.SetActive(false);
        }
        levelPrefabs[roundNumber].gameObject.SetActive(true);
    }

    // Round ended. 
    // What are the victory conditions?
    // Goal Reached?
    public void EndRound(){
        EndRound(true); // calls the other end round function
    }
    public void EndRound( bool roundWasWon ){
        if(roundWasWon){
            // Round was won
            currentRoundCounter++;
            if(currentRoundCounter < levelPrefabs.Count){
                CreateRound(currentRoundCounter);
            }else{
                GameCompleted();
            }

        } else{
            // Round was Lost
        }
	}

    // **** Game Controls ****
    public void StartGame() {
        // initialize game settings
        currentLivesLeft = playerLives;
        currentRoundCounter = 0;
        currentScore = 0;

        // create a new player
        RespawnPlayer();


        // create new level 
        CreateRound(0);// 0 is the first round

        // reset scores etc

        // turn on UI, set new values
    }

    // All lives are lost? 
    public void GameOver(){
		Debug.Log("Game Ended!");
	}

    public void GameCompleted(){
        Debug.Log("You Win");
    }

    public void UpdateUI(){
        
        if (hud != null) {
            hud.UpdateUI(currentPlayer);
        } else {
            Debug.LogWarning("No HUD found on Game Manager");
        }
    }
}
