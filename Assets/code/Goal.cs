﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Goal : MonoBehaviour {

	public UnityEvent goalReachedEvent;

    // reference to the Game Manager
    private GameManager manager;

    [Header("Debug Settings")]
    public bool debug_goal = false;

	//public void OnCollisionEnter2D(Collision2D col){
	public void OnTriggerEnter2D(Collider2D col){
        // get the player controller reference
        PlayerController player = col.GetComponent<PlayerController>();
        // if we collided with a player
        if ( player != null){
            // Debugging
            if(debug_goal ){
                Debug.Log("DEBUG: Goal Collided with " + col.gameObject.name);
            }
            
            // call our goal event
            goalReachedEvent.Invoke();

            //manager 
            manager.EndRound();
        }
    }

	// Use this for initialization
	void Start () {
        // Find is hacky and error prone but simple
        manager = GameObject.FindObjectOfType<GameManager>();
        if( manager == null){
            Debug.LogWarning("No Game Manager Found in the scene by " + gameObject.name);
        }
    }
}
