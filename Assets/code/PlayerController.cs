﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This controls the player
public class PlayerController : MonoBehaviour {

    [Header("*** Input ***")]
    public bool buttonDown = false;
    // set up a vector 2 to hold the input from the player
    public Vector2 inputVector = Vector2.zero;

    // *** Player State ***
    public float movementForce = 1f;

    // Other Classes on the GameObject
    // The Rigidbody is needed to move items within the physics system
    public Rigidbody2D playerRigidbody;

    [Header(" *** Player State Information ***")]
    // public Sprite portrait;
    // public string playerName;
    public int collectedCoins = 0;

    // is the player activated or not? 
    private bool isActivated = true;

    [Header("Player Settings")]
    public bool destroyOnDeath = false;

    [Header("Stored Class References")]
    private GameManager manager;

    [Header("Debug")]
    public bool printDebugs = false;
    // *** Player State Functions ***
    public void CollectCoin() {
        collectedCoins++;
    }

    // remove a coin from the players collection
    public void LoseCoin() {
        collectedCoins--;
    }

    public void HitTrap(){
        if(printDebugs ){
            Debug.Log("PlayerHitTrap");
        }

        // no health yet
            manager.RespawnPlayer();
        
        if( destroyOnDeath ){
            // destroy the player
            Destroy(gameObject);
        } else{
            // deactivate the player
            isActivated = false;
        }
    }

    public void Initialize(GameManager gm){
        manager = gm;
    }

    // Use this for initialization - It is called when the object starts for the first time
    void Start () {
        // grab the physics object so we can move it
         playerRigidbody = GetComponent<Rigidbody2D>();

        // reset coins
        collectedCoins = 0;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if(isActivated){
            // Gets the Jump Button
            buttonDown = Input.GetButton("Jump");
            // you can also use .GetButtonDown("Jump") to only only register true on the single frame it was hit, useful for UI's

            // Gets the input values from the arrow keys (by default)
            inputVector = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
            // this will return us a value for x and y between -1 and 1
            // you can also do them seperately and handle the x axis and y axis each on thier own float

            // We're using Physics so put it in Fixed Update rather then Update
            // Physics - player
            playerRigidbody.AddForce(inputVector * movementForce * Time.deltaTime);
        }
    }
}
