﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events; // Added this to use the UnityEvents 

public class Coin : MonoBehaviour {

	public UnityEvent coinCollectedEvent; // for adding additional effects

    // reference to the Game Manager
    private GameManager manager;

    [Header("Settings")]
    public bool disableWhenCollected = true;

    [Header("Effects")]
    // The audio Source that plays the sound. Note we can't disable this before the sound is done playing or it will cut off. 
    public AudioSource collectedSound;
    // this is the sprite reference. We are disabling this but you could easily change it to a new sprite as well
    // eg. art.sprite = newSprite; // if newSprite is type Sprite;
    private SpriteRenderer art;

    [Header("Debug Settings")]
    public bool debug_coin = false;

	//public void OnCollisionEnter2D(Collision2D col){
	public void OnTriggerEnter2D(Collider2D col){
        // get the player controller reference
        PlayerController player = col.GetComponent<PlayerController>();
        // if we collided with a player
        if ( player != null){
            // Debugging
            if(debug_coin) {
                Debug.Log("DEBUG: Coin Collided with " + col.gameObject.name);
            }

            // Add one coin to the player counter
            player.CollectCoin();
            manager.UpdateUI();

            // Play Collection Sound
            if (collectedSound != null){
                collectedSound.Play();
            }

            // disable the coin
            if(disableWhenCollected){
                this.enabled = false;
                if( art != null){
                    art.enabled = false;
                }
            }

            // call our goal event, ignore this if you don't have any events
                coinCollectedEvent.Invoke();
        }
	}

	// Use this for initialization
	void Start () {
        // Find is hacky and error prone but simple, will grab the first object of type GameManager it can find in the scene (must be active)
        manager = GameObject.FindObjectOfType<GameManager>();
        // Get Component is better. This one will find the first object of type 'SpriteRenderer' that is on the gameobject. 
        art = GetComponent<SpriteRenderer>();

        if ( manager == null){
            Debug.LogWarning("No Game Manager Found in the scene by " + gameObject.name);
        }
    }
}
