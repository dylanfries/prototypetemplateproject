﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Trap : MonoBehaviour {

    // events are more advanced, but you can use them to add arbitrary effects to the Trap being triggered (in the scene)
	public UnityEvent trapReachedEvent;

    [Header("Audio")]
    public AudioSource trapHitSound;

    [Header("Trap Settings")]
    public bool disableOnHit = false;

    [Header("Debug Settings")]
    public bool debug_trap = false;

    // If the Collider attached to this script has a Collider with the Trigger checkbox checked, 
    // call OnTriggerEnter2D with the other colliders reference
	public void OnTriggerEnter2D(Collider2D col){

        // get the player controller reference from the collider. 
        // We are assuming the PlayerController script is on the same GameObject as the collider
        PlayerController player = col.GetComponent<PlayerController>();

        // if we collided with a player
        if ( player != null){
            // Debugging
            if(debug_trap) {
                Debug.Log("DEBUG: Trap Collided with " + col.gameObject.name);
            }

            // call our goal event
            trapReachedEvent.Invoke();

            // apply damage to the player
            player.HitTrap();

            // Check trap settings 
            if( disableOnHit ){
                gameObject.SetActive(false);
            }

            // If we have a trap hit sound, play it
            if(trapHitSound != null){
                trapHitSound.Play();
            }
        }
	}
}
